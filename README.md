# OpenSEES剪力墙

#### 介绍

OpenSEES剪力墙数值模拟: MVLEM SFI-MVLEM LayerShell

RW2为剪力墙构件，SW1为联肢剪力墙结构

本项目已与其他项目归并，不再维护，请移步[OpenSEES实例集锦](https://gitee.com/civilwilson/OpenSEES_Examples)

#### 模型入口

单元类型 | 实例类型 | 位置
:-------------: | :-------------: | :-------------: 
MVLEM  | 剪力墙构件 | RW2\\MVLEM.tcl 
MVLEM  | 联肢剪力墙结构 | SW1\\SW1.tcl
SFI-MVLEM  | 剪力墙构件 | RW2\\SFI-MVLEM.tcl 
SFI-MVLEM  | 联肢剪力墙结构 | SW1\\SW1.tcl 
LayerShell  | 剪力墙构件 | RW2\\LayerShell.tcl 
LayerShell  | 剪力墙构件 * 2 | 谢博士模型\\ 
LayerShell  | 联肢剪力墙结构 | SW1\\SW1.tcl 

[建议采用OSRun进行分析](https://gitee.com/civilwilson/OSRun)：
[OSRun相关推送](https://mp.weixin.qq.com/s/7JRAqb0eAxRUYObryaDymA)

#### 相关推送

1.  [MVLEM](https://mp.weixin.qq.com/s/fEic1x5EpGZSPD9MgcqPsw)

2.  [SFI-MVELM](https://mp.weixin.qq.com/s/oGby_sg2lyYTUS4BFEKSMg)

3.  [LayerShell](https://mp.weixin.qq.com/s/J_BIB0tQUJUJcstbkVf_tg)

4.  [模拟实例](https://mp.weixin.qq.com/s/vVFQFV0GlExflwkVfBQscg)
