wipe
model basic -ndm 2 -ndf 3
source Wilson_Proc.tcl

##Nmeber of Elements
set ElementNum 6
##Height
set H 3658.
##Width 
set B 1219.
##Thickness
set T 102.
##Boundary Steel Ratio
set sro1 0.0293
##Central Steel Ratio
set sro2 0.0033
## Axial Force Ratio
set u 0.07
##Compressive strength of Concrete
set fc 42.85

set A [expr $B * $T]
set deltaH [expr $H / $ElementNum]

puts "Node"
for { set i  0} { $i <= $ElementNum } { incr i} {
    set Nodeid [expr $i + 1]
    set Nodev [expr $deltaH * $i]
    node $Nodeid 0 $Nodev   
}

fix 1 1 1 1

puts "Material"
##Boundary Vertical Steel
uniaxialMaterial Steel02 1 414 2E5  0.02 20 0.925 0.15
##Central Vertical Steel
uniaxialMaterial Steel02 2 449 2E5  0.02 20 0.925 0.15
##Confined Concrete
uniaxialMaterial Concrete02 3 -45.32 -0.0026 -22.66 -0.0085 0.1 4.06 1000
##UnConfined Concrete
uniaxialMaterial Concrete02 4 -42.85 -0.002 -8.67 -0.008 0.1 4.06 1000

set G 1.2E4
set GAs [expr $G * $A * 0.5]
uniaxialMaterial Elastic 5 $GAs

puts "Element"
for {set i 1 } { $i <= $ElementNum } { incr i } {
    set b1 191
    set b2 36.5
    set nodei [expr $i ]
    set nodej [expr $i + 1]
    element MVLEM $i 0 $nodei $nodej 8 0.4 -thick $T $T $T $T $T $T $T $T -width $b1 $b2 $b1 $b1 $b1 $b1 $b2 $b1 -rho $sro1 0 $sro2 $sro2 $sro2 $sro2 0 $sro1 -matConcrete 3 4 4 4 4 4 4 3 -matSteel 1 2 2 2 2 2 2 1 -matShear 5
}

puts "Gravity"
set LoadNode [expr $ElementNum + 1 ]
set GLoad [expr $u * $fc * $A ]
pattern Plain 1 Linear {
    load $LoadNode 0 -$GLoad 0
}
Gravity_Proc 10

pattern Plain 2 Linear {
    load $LoadNode 1E3 0 0
}

recorder Node -file F_Disp.txt -time -node $LoadNode -dof 1 disp
recorder Element -file Strain.txt -ele 1 Fiber_Strain

puts "Cyclic"
set DT {10 22 24 40 55 70}
foreach dt $DT {
    Cyclic_Proc $dt 1 0.5 $LoadNode 1
}
wipe





