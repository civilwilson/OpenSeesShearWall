wipe
model basic -ndm 3 -ndf 6
source Wilson_Proc.tcl

##Nmeber of Elements in Z.
set ElementNum 18
##Nmeber of Elements in X.
set Horizontal 6
##Height
set H 3658.
##Width 
set B 1219.
##Thickness
set T 102.
##Boundary Horizontal Steel Ratio 
set rouX1 0.005
##Central Steel Ratio
set rouX2 0.0032
##Boundary Steel Ratio
set sro1 0.0293
##Central Steel Ratio
set sro2 0.0033
##Concrete Friction Coefficient
set u 0.07
##Compressive strength of Concrete
set fc 42.85
##Time to start the program

set A [expr $B * $T]
set deltaH [expr $H / $ElementNum]
set deltaX [expr $B / $Horizontal]

puts "Node"
for { set i  0} { $i <= $ElementNum } { incr i} {
    for {set ii 1} { $ii <= $Horizontal +1 } {incr ii} {
        set Nodeid [expr ($i + 1) * 100 + $ii]
        set Nodey [expr $deltaH * $i]
        set Nodex [expr ($ii - 1 ) * $deltaX ]
        node $Nodeid $Nodex 0 $Nodey
        if {$Nodey != 0} {
            fix $Nodeid 0 1 0 1 0 1
        }   
    }
    
}

fixZ 0 1 1 1 1 1 1

puts "Material"
##Confined nDmaterial PlaneSress
nDMaterial PlaneStressUserMaterial 3 40 7 42.85 4.06 -8.67 -0.002 -0.008 0.00025 0.05
nDMaterial PlateFromPlaneStress 6 3 1.2E4

##UnConfined nDmaterial PlaneSress
nDMaterial PlaneStressUserMaterial 4 40 7 42.85 4.06 -8.67 -0.002 -0.008 0.00025 0.05
nDMaterial PlateFromPlaneStress 7 4 1.2E4

##Boundary Vertical Steel
uniaxialMaterial Steel02 1 414 2E5  0.001 20 0.925 0.15
nDMaterial PlateRebar 8 1 90

##Central Vertical Steel and Horizontal Steel
uniaxialMaterial Steel02 2 449 2E5  0.001 20 0.925 0.15
nDMaterial PlateRebar 9 2 90
nDMaterial PlateRebar 10 2 0

puts "Layer"
set HLayerT1 [expr $rouX1 * $T / 2]
set HLayerT2 [expr $rouX2 * $T / 2]

section LayeredShell 1 7 6 20 10 $HLayerT1  6 20 6 20 6 20  10 $HLayerT1 6 20
section LayeredShell 2 7 7 20 10 $HLayerT1  7 20 7 20 7 20  10 $HLayerT1 7 20

puts "Element"
for {set i 1 } { $i <= $ElementNum } { incr i } {
    for {set ii 1 } {$ii <= $Horizontal } {incr ii} {
        set nodei [expr $i * 100 + $ii]
        set nodej [expr $i * 100 + $ii + 1 ] 
        set nodek [expr ($i + 1) * 100 + $ii + 1 ] 
        set nodel [expr ($i + 1) * 100 + $ii] 
        set eleid $nodei
        if { $ii == 1 } {
            element ShellMITC4 $eleid $nodei $nodej $nodek $nodel 1
        } elseif {$ii == $Horizontal} {
            element ShellMITC4 $eleid $nodei $nodej $nodek $nodel 1
        } else {
            element ShellMITC4 $eleid $nodei $nodej $nodek $nodel 2 
        }
    }
}

set As1 [expr $sro1 * $deltaX * $T / 2]
set As2 [expr $sro2 * $deltaX * $T]
set TrussID 10000
for {set i 1 } {$i <= $ElementNum } {incr i} {
    for {set ii 1 } {$ii <= $Horizontal + 1 } {incr ii} {
        set nodei [expr $i * 100 + $ii]
        set nodej [expr ($i+1) * 100 + $ii]
        if { $ii <= 2} {
            element truss $TrussID $nodei $nodej $As1 1
        } elseif {$ii >= $Horizontal} {
            element truss $TrussID $nodei $nodej $As1 1
        } else {
            element truss $TrussID $nodei $nodej $As2 2
        }
        incr TrussID
    }
}

puts "Gravity"
set GLoad [expr $u * $fc * $A  / ($Horizontal + 1)]
pattern Plain 1 Linear {
    for {set i 1} {$i <= $Horizontal + 1 } { incr i } {
        set LoadNode [expr ($ElementNum + 1) * 100 + $i ]
        load $LoadNode 0 0 -$GLoad 0 0 0
    }
}

Gravity_Proc 10

set LoadNode [expr ($ElementNum + 1) * 100 + int($Horizontal/2)]
pattern Plain 2 Linear {
    load $LoadNode 1E3 0 0 0 0 0
}

recorder Node -file F_Disp.txt -time -node $LoadNode -dof 1 disp
recorder Node -file Strain.txt -node 201 -dof 3 disp

puts "Cyclic"
set DT {10 22 24 40 55 70}
foreach dt $DT {
    Cyclic_Proc $dt 1 0.5  $LoadNode 1 1E-3 2000
}
wipe





