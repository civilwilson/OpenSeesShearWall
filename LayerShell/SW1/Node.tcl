puts "Nodes"

for {set Storey 1 } { $Storey <= 6 }  { incr Storey } {
    set NodeTag1 [expr { $Storey *100 + 1 }]
    set NodeTag2 [expr { $Storey *100 + 2 }]
    set NodeTag3 [expr { $Storey *100 + 3 } ] 
    set NodeTag4 [expr { $Storey *100 + 4 } ] 
    set NodeTag5 [expr { $Storey *100 + 5 } ] 
    set NodeTag6 [expr { $Storey *100 + 6 } ] 

    set NodeTag7 [expr { $Storey *100 + 7 } ] 

    set Nodey [expr { ($Storey - 1) * 300 }]
    set Nodex1 -390 ; set Nodex2 -90 ;set Nodex5 -240
    set Nodex3 [expr { $Nodex2 * -1 }]  
    set Nodex4 [expr { $Nodex1 * -1 }]  
    set Nodex6 [expr { $Nodex5 * -1 }]

    node $NodeTag1 $Nodex1 0 $Nodey
    node $NodeTag2 $Nodex2 0 $Nodey
    node $NodeTag3 $Nodex3 0 $Nodey
    node $NodeTag4 $Nodex4 0 $Nodey
    node $NodeTag5 $Nodex5 0 $Nodey
    node $NodeTag6 $Nodex6 0 $Nodey

    if {$Nodey != 0 } {
        node $NodeTag7 0 0 $Nodey
    }
}

for {set Storey 1 } { $Storey <= 5 }  { incr Storey } {
    set NodeTag1 [expr { ($Storey + 6) *100 + 1 }]
    set NodeTag2 [expr { ($Storey + 6)*100  + 2 }]
    set NodeTag3 [expr { ($Storey + 6)*100  + 3 } ] 
    set NodeTag4 [expr { ($Storey + 6)*100  + 4 } ] 
    set NodeTag5 [expr { ($Storey + 6)*100  + 5 } ] 
    set NodeTag6 [expr { ($Storey + 6)*100  + 6 } ] 

    set Nodey [expr { ($Storey - 1) * 300 + 150 }]
    set Nodex1 -390 ; set Nodex2 -90 ;set Nodex5 -240
    set Nodex3 [expr { $Nodex2 * -1 }]  
    set Nodex4 [expr { $Nodex1 * -1 }]  
    set Nodex6 [expr { $Nodex5 * -1 }]  

    node $NodeTag1 $Nodex1 0 $Nodey
    node $NodeTag2 $Nodex2 0 $Nodey
    node $NodeTag3 $Nodex3 0 $Nodey
    node $NodeTag4 $Nodex4 0 $Nodey
    node $NodeTag5 $Nodex5 0 $Nodey
    node $NodeTag6 $Nodex6 0 $Nodey
}

for {set Storey 1 } { $Storey <= 5 }  { incr Storey } {
    set NodeTag1 [expr { ($Storey + 11) *100 + 1 }]
    set NodeTag2 [expr { ($Storey + 11)*100  + 2 }]
    set NodeTag3 [expr { ($Storey + 11)*100  + 3 } ] 
    set NodeTag4 [expr { ($Storey + 11)*100  + 4 } ] 
    set NodeTag5 [expr { ($Storey + 11)*100  + 5 } ] 
    set NodeTag6 [expr { ($Storey + 11)*100  + 6 } ] 

    set NodeTag7 [expr { ($Storey + 11) *100 + 7 } ] 

    set Nodey [expr { ($Storey - 1) * 300 + 260 }]
    set Nodex1 -390 ; set Nodex2 -90 ;set Nodex5 -240
    set Nodex3 [expr { $Nodex2 * -1 }]  
    set Nodex4 [expr { $Nodex1 * -1 }]  
    set Nodex6 [expr { $Nodex5 * -1 }]  

    node $NodeTag1 $Nodex1 0 $Nodey
    node $NodeTag2 $Nodex2 0 $Nodey
    node $NodeTag3 $Nodex3 0 $Nodey
    node $NodeTag4 $Nodex4 0 $Nodey
    node $NodeTag5 $Nodex5 0 $Nodey
    node $NodeTag6 $Nodex6 0 $Nodey

    if {$Nodey != 0 } {
        node $NodeTag7 0 0 $Nodey
    }
}


puts "Constraint"
fixZ 0. 1 1 1 1 1 1 