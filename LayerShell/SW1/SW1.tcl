wipe

puts "System"
model basic -ndm 3 -ndf 6
source Wilson_Proc.tcl

#建立节点并施加底部约束
source Node.tcl

#材料本构定义
source Material.tcl
#source FiberSection.tcl

#单元定义（含纤维截面）
source Element.tcl

Gravity_Proc 10

recorder Node -file F-Disp.txt -time -node 602 -dof 1 disp


puts "Pushover"
pattern Plain 2 Linear {
load 602 1E3 0 0 0 0 0
}

set DT {4 7 12 18 22 27 32 36 42 47 51 57 }
foreach dt $DT {
    Cyclic_Proc $dt 1 1 602 1 1E-2 1000
}

wipe